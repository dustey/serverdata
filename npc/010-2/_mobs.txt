// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 010-2: Desert Mountains mobs
010-2,173,32,9,9	monster	Maggot	1030,10,35000,150000
010-2,159,51,9,9	monster	Maggot	1030,8,35000,150000
010-2,122,102,8,2	monster	Snake	1122,2,35000,300000
010-2,163,77,3,8	monster	Mountain Snake	1123,7,35000,300000
010-2,98,122,3,2	monster	Desert Bandit	1124,1,35000,300000
010-2,110,113,3,2	monster	Desert Bandit	1124,1,35000,300000
010-2,126,110,3,2	monster	Desert Bandit	1124,2,35000,300000
010-2,141,102,6,5	monster	Desert Bandit	1124,3,35000,300000
010-2,158,57,6,5	monster	Desert Bandit	1124,3,35000,300000
010-2,149,95,6,5	monster	Sarracenus	1125,2,35000,300000
010-2,149,83,3,8	monster	Mountain Snake	1123,6,35000,300000
010-2,49,107,3,8	monster	Mountain Snake	1123,7,35000,300000
010-2,78,120,3,8	monster	Mountain Snake	1123,2,35000,300000
010-2,178,88,3,8	monster	Mountain Snake	1123,3,35000,300000
010-2,172,92,3,8	monster	Snake	1122,4,35000,300000
010-2,154,81,29,46	monster	Desert Maggot	1083,35,35000,150000
010-2,86,120,29,22	monster	Desert Maggot	1083,20,35000,150000
