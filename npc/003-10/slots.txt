// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Slot Machine for bets

003-10,22,37,0	script	Slot Machine#003-10a	NPC_SLOTMACHINE,{
    function symbol{
        switch (getarg(0)) {
        case 1:
            mesn "%%A";
            break;
        case 2:
            mesn "%%B";
            break;
        case 3:
            mesn "%%C";
            break;
        case 4:
            mesn "%%D";
            break;
        case 5:
            mesn "%%E";
            break;
        case 6:
            mesn "%%F";
            break;
        case 7:
            mesn "7";
            break;
        default:
            mesn "%%@";
            break;
        }
    }

L_Menu:
    mesn;
    mes col("Spin three symbols, and jackpot great rewards!", 9);
    mes col("Just one coin for spin.", 9);
    next;
    menu
        rif(countitem(CasinoCoins) >= 1, l("Spin!")), L_Spin,
        l("Prizes"), L_Info,
        l("Trade"), L_Trade,
        l("Leave"), L_Quit;

L_Info:
    mes "";
    mes col("Prizes:", 9);
    mes l("##9 777: @@.", getitemlink(Monocle));
    mes col("Three equal: 30 coins.", 9);
    mes col("Two equal: 1 coin.", 9);
    next;
    goto L_Menu;


L_Spin:
    mes col("Spinning...", 9);
    next;
    delitem CasinoCoins, 1;
    .@a=rand(1,7);
    .@b=rand(1,7);
    .@c=rand(1,7);
    symbol(.@a);
    symbol(.@b);
    symbol(.@c);
    next;
    mesn;
    if (.@a == .@b && .@a == .@c && .@a == 7) {
        getitem Monocle, 1;
        mes col("Jackpot! You got the Monocle!", 3);
    } else if (.@a == .@b && .@a == .@c) {
        getitem CasinoCoins, 30;
        mes col("Congrats! You got thirty coins!", 3);
    } else if (.@a == .@b || .@a == .@c || .@b == .@c) {
        getitem CasinoCoins, 1;
        mes col("Lucky! You got a coin!", 3);
    } else {
        mes col("It wasn't this time...", 3);
    }
    next;
    goto L_Menu;

L_Trade:
    openshop;
    closedialog;
    close;

L_Quit:
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
	tradertype(NST_CUSTOM);

    sellitem WoodenSword, 940;
    sellitem JeansShorts, 820;
	sellitem ElixirOfLife, 150;
    sellitem PoisonArrow, 30;
    sellitem StrangeCoin, 5;
    end;

OnCountFunds:
	setcurrency(countitem(CasinoCoins));
	end;

OnPayFunds:
	if( countitem(CasinoCoins) < @price )
		end;
	delitem CasinoCoins,@price;
	purchaseok();
	end;


}

