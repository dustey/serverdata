// Author:
//    Saulc
// Notes: Eomie did the bug bomb at Candor

003-1,68,24,0	script	Eomie	NPC_ELF_F,{

hello;

OnInit:
    .@npcId = getnpcid(0, "Eomie");
    //setunitdata(.@npcId, UDT_HEADTOP, 2929);
    setunitdata(.@npcId, UDT_HEADMIDDLE, 1319);
    setunitdata(.@npcId, UDT_HEADBOTTOM, 2207);
    //setunitdata(.@npcId, UDT_WEAPON, 1802); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 12);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
