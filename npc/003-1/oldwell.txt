// TMW-2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Free well that give sewer water. a good place for tulim kids to miss school :b

003-1,53,144,0	script	Old Well#003-1	NPC_NO_SPRITE,{

    mes l("##9You found an old well with a bucket on it! It's time to fill plenty of @@!", getitemlink(EmptyBottle));
    input .@count;

    if (.@count == 0)
        close;
    .@empty = countitem(EmptyBottle);

    if (.@empty < .@count)
        goto L_NotEnoughBottles;
    getinventorylist;
    inventoryplace BottleOfSewerWater, .@count;

    delitem EmptyBottle, .@count;
    getitem "BottleOfSewerWater", .@count;
    dispbottom("Eek, Sewer Water! What the?! Better not drink this!");
    close;

L_NotEnoughBottles:
    mes col("You don't have that many empty bottles!", 9);
    close;


OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

